﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using XBatterySample.Core;

namespace XBatterySample.Droid
{
    [Activity(Label = "XBatterySample.Droid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        IBatteryService _batteryService;
        TextView _batteryConnectedIndicator;
        View _batteryView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            _batteryConnectedIndicator = FindViewById<TextView>(Resource.Id.batteryIndicatorTextView);
            _batteryView = FindViewById<View>(Resource.Id.chargingView);

            _batteryService = new BatteryService();

            SetLabelIndicator(_batteryService.IsPhoneCharging);
        }

        protected override void OnResume()
        {
            base.OnResume();
            _batteryService.PowerSourceChanged += OnPowerSourceChanged;
        }

        protected override void OnPause()
        {
            base.OnPause();
            _batteryService.PowerSourceChanged -= OnPowerSourceChanged;
        }

        void OnPowerSourceChanged (object sender, bool e)
        {
            SetLabelIndicator(e);
        }

        void SetLabelIndicator(bool charging)
        {
            _batteryConnectedIndicator.Text = string.Format("Battery is Charging.... {0}", charging);

            _batteryView.Background = charging ? 
                Resources.GetDrawable( Android.Resource.Color.HoloGreenLight) : 
                Resources.GetDrawable( Android.Resource.Color.HoloRedLight);
        }
    }
}


