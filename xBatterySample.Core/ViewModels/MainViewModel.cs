﻿using System;
using GalaSoft.MvvmLight;

//Resharper disable once CheckNamespace
namespace XBatterySample.Core.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public bool IsCharging { get; set; }

        private readonly IBatteryService _batteryService;

        public MainViewModel(IBatteryService batteryService)
        {
            _batteryService = batteryService;
        }

        public void Init()
        {
            IsCharging = _batteryService.IsPhoneCharging;
        }

        public void SetBindings()
        {
            _batteryService.PowerSourceChanged += OnPowerSourceChanged;            
        }

        public void UnSetBindings()
        {
            _batteryService.PowerSourceChanged -= OnPowerSourceChanged;            
        }

        void OnPowerSourceChanged (object sender, bool e)
        {
            IsCharging = e;
        }
    }
}