﻿using System;

namespace XBatterySample.Core
{
    /// <summary>
    /// This is a wrapper for the Cross-Platform pluggin to manage the Battery states.
    /// </summary>
    public interface IBatteryService
    {
        /// <summary>
        /// Occurs when power source changed.
        /// </summary>
        event EventHandler<bool> PowerSourceChanged;

        /// <summary>
        /// Gets a value indicating whether the phone is currently charging.
        /// </summary>
        /// <value><c>true</c> if this instance is phone charging; otherwise, <c>false</c>.</value>
        bool IsPhoneCharging { get;}

        /// <summary>
        /// Listens for battery change.
        /// </summary>
        void ListenForBatteryChange();

        /// <summary>
        /// Stops the listenin for battery change.
        /// </summary>
        void StopListeningForBatteryChange();
    }

}

