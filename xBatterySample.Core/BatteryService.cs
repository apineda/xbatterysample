using System;
using Plugin.Battery;
using Plugin.Battery.Abstractions;

namespace XBatterySample.Core
{
    /// <summary>
    /// This is the default implementation of the <see cref="IBatteryService"/>
    /// </summary>
    public class BatteryService : IBatteryService
    {
        private PowerSource _powerSource;

        public bool IsPhoneCharging
        { 
            get
            {
                return CrossBattery.Current.PowerSource != PowerSource.Battery; 
            }
        }

        public event EventHandler<bool> PowerSourceChanged;

        public BatteryService()
        {
            _powerSource = CrossBattery.Current.PowerSource;

            ListenForBatteryChange();
        }

        public void ListenForBatteryChange()
        {
            StopListeningForBatteryChange();

            CrossBattery.Current.BatteryChanged += RaiseBatteryPowerSourceChanged;
        }

        public void StopListeningForBatteryChange()
        {
            CrossBattery.Current.BatteryChanged -= RaiseBatteryPowerSourceChanged;
        }

        void RaiseBatteryPowerSourceChanged (object sender, BatteryChangedEventArgs e)
        {
            if (_powerSource != e.PowerSource)
            {
                _powerSource = e.PowerSource;
                PowerSourceChanged?.Invoke(sender, IsPhoneCharging);
            }
        }

    }
}
